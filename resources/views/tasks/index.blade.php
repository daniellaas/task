@extends('layouts.app')
@section('content')
<h1> This is your task list </h1>
    @if(Request::is('tasks'))
    <a href = "{{route('myfilter')}}">My tasks</a>
    @else
     <a href = "{{route('tasks.index')}}">All Tasks</a>
    @endif

<ul>
    @foreach($tasks as $task)
    <li>
    title: {{$task->title}}
    
    <a href = "{{route('tasks.edit',$task->id)}}">  Edit </a>
    @can('admin')
    <a href = "{{route('delete',$task->id)}}">  Delete </a>
    @endcan
    @if ($task->status == 0)
        @can('admin')
         <a href="{{route('done', $task->id)}}">Mark As done</a>
        @endcan     
    @else
        Done!
    @endif

</form>
    </li>
    @endforeach
</ul>

<a href = "{{route('tasks.create')}}">Create a new task</a>
@endsection