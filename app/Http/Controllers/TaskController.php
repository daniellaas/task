<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
        $tasks = Task::all();
        return view('tasks.index',['tasks'=>$tasks]); 
        }
        return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
        return view ('tasks.create');
        }
        return redirect()->intended('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $tasks=new Task();
            $id = Auth::id();
            $tasks->title=$request->title;
            $tasks->status=0;
            $tasks->user_id=$id;
            $tasks->save();
    
        return redirect('tasks');
        }
        return redirect()->intended('/home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        $task= Task::find($id);
        return view ('tasks.edit', compact('task'));
        }
        return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
        $task = Task::find($id);
        $task->title=$request->title;
        $task->save();
        return redirect('tasks');
        }
        return redirect()->intended('/home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");
       } 
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');
        }
        return redirect()->intended('/home');
    }

    public function done($id)
    {
        if (Auth::check()) {
        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");
        } 
        $task = Task::findOrFail($id); 
        $task->status = 1;
        $task->save();
        return redirect('tasks');
        }
        return redirect()->intended('/home');
    }

    public function myfilter()
    {
        if (Auth::check()) {
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = $user->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
        }
        return redirect()->intended('/home');
    }
}
